# After pay Credit Card Fraud Detection Algorithm

## Assumptions
 * All transactions has a hashed credit card number.
 * It is expected to find out the fraudulent credit card numbers per given date
 * Price of the transaction can be positive and negative.
 * Transaction timestamp and Validate date are in the same timezone.
 * Validation date has yyyy-mm-dd format
 * It is expected to use only Java with out any J2EE frameworks
 
## Structure
 * The main logic is implemented in `FraudCreditCardDetectionService` class
 * Input validation is implemented in `ServiceValidator`

## How to build
 * Run `mvn clean install -D skipTests`
 * Run `mvn clean install` to build and run the tests
 
## How to run and test
 * `mvn test` will run all the unit tests
 * `mvn verify` will run all unit + integration test
 
## Author
 * Buddhika Semasinghe `buddhika.semasinghe@gmail.com`
