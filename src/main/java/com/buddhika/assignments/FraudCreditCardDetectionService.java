package com.buddhika.assignments;

import com.buddhika.assignments.exception.FraudCreditCardDetectionException;
import com.buddhika.assignments.model.Transaction;
import com.buddhika.assignments.util.ServiceValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by Buddhika Semasinghe
 */
public class FraudCreditCardDetectionService {
    final static Logger LOGGER = LoggerFactory.getLogger(FraudCreditCardDetectionService.class);
    final static int NO_OF_HOURS_PER_DAY = 24;

    private ServiceValidator serviceValidator;

    public void setServiceValidator(ServiceValidator serviceValidator) {
        this.serviceValidator = serviceValidator;
    }

    public List<String> process(final List<String> transactionRecords,
                                final String validationDate,
                                final BigDecimal priceThreshold) throws FraudCreditCardDetectionException {

        LocalDate dateToValidateAgainst = serviceValidator.validateDate(validationDate);
        LocalDateTime startDatetime = dateToValidateAgainst.atStartOfDay();
        LocalDateTime endDateTime = startDatetime.plusHours(NO_OF_HOURS_PER_DAY);
        List<Transaction> transactions = serviceValidator.validateTransactionRecords(transactionRecords);

        LOGGER.info("Number of Transactions compared against {}", transactions.size());

        // Build the map of cardNumber -> totalTransaction amount filtered by the given date
        Map<String, BigDecimal> sumOfTheTransactionsPerGivenDate = transactions.stream()
                .filter(transaction -> transaction.getTransactionDatetime().isAfter(startDatetime)
                        && transaction.getTransactionDatetime().isBefore(endDateTime))
                .collect(Collectors.groupingBy(Transaction::getCardNumber,
                        Collectors.mapping(Transaction::getPrice,
                                Collectors.reducing(
                                        BigDecimal.ZERO,
                                        (sum, ele) -> sum.add(ele)))));

        LOGGER.info("sumOfTheTransactionsPerGivenDate {} ", sumOfTheTransactionsPerGivenDate.size());

        // Check against the price Threshold
        List<String> fraudulentCreditCardNumbers = sumOfTheTransactionsPerGivenDate.entrySet().stream()
                .filter(map -> map.getValue().compareTo(priceThreshold) > 0)
                .map(map -> map.getKey())
                .collect(Collectors.toList());

        LOGGER.info("Count of list of  fraudulentCreditCardNumbers {} ", fraudulentCreditCardNumbers.size());
        return fraudulentCreditCardNumbers;
    }
}
