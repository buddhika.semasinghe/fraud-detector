package com.buddhika.assignments.util;

import com.buddhika.assignments.exception.InvalidTransactionRecordFormat;
import com.buddhika.assignments.exception.InvalidValidationDate;
import com.buddhika.assignments.model.Transaction;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by Buddhika Semasinghe
 */
public class ServiceValidator {
    final static Logger LOGGER = LoggerFactory.getLogger(ServiceValidator.class);
    private static final int TOKEN_COUNT = 3;

    public List<Transaction> validateTransactionRecords(List<String> transactionRecords)
            throws InvalidTransactionRecordFormat {
        if (CollectionUtils.isEmpty(transactionRecords)) {
            LOGGER.error("TransactionRecord List is empty");
            throw new InvalidTransactionRecordFormat("Null or Empty input Transactions");
        }

        List<Transaction> transactions = new ArrayList<>();
        transactionRecords.stream().forEach(recordString -> {
            tokenize(recordString).ifPresent(transaction -> transactions.add(transaction));
        });
        return transactions;
    }

    public LocalDate validateDate(String validationDate) throws InvalidValidationDate {
        if (StringUtils.isEmpty(validationDate)) {
            LOGGER.error("Validate date is empty");
            throw new InvalidValidationDate("Empty or Null Compared Date");
        }

        try {
            return LocalDate.parse(validationDate, DateTimeFormatter.ISO_DATE);
        } catch (DateTimeParseException e) {
            LOGGER.error("Invalid date format for Validation Date {} ", validationDate);
            throw new InvalidValidationDate("Invalid date format: Expecting DateTimeFormatter.ISO_DATE");
        }
    }

    private Optional<Transaction> tokenize(String recordString) {
        String[] tokenizedStrings = StringUtils.split(recordString, ',');
        if (tokenizedStrings.length != TOKEN_COUNT) {
            LOGGER.error("Invalid Transaction Record format {} ", tokenizedStrings.length);
            return Optional.empty();
        }

        Transaction transaction = new Transaction();
        String creditCardNumber = tokenizedStrings[0];
        String txDateString = tokenizedStrings[1];
        String price = tokenizedStrings[2];

        if (StringUtils.isEmpty(creditCardNumber)) {
            LOGGER.warn("Empty Credit Card Hash");
            return Optional.empty();
        }
        transaction.setCardNumber(creditCardNumber.trim());

        if (StringUtils.isEmpty(txDateString)) {
            LOGGER.warn("Empty Transaction Date");
            return Optional.empty();
        }

        try {
            LocalDateTime transactionDateTime = LocalDateTime.parse(txDateString.trim(), DateTimeFormatter.ISO_LOCAL_DATE_TIME);
            transaction.setTransactionDatetime(transactionDateTime);
        } catch (DateTimeParseException e) {
            LOGGER.warn("Unsupported Transaction Date time format {} ", txDateString);
            return Optional.empty();
        }

        try {
            transaction.setPrice(new BigDecimal(price.trim()));
        } catch (NumberFormatException e) {
            LOGGER.warn("Incorrect transaction amount {} ", price);
            return Optional.empty();
        }
        return Optional.of(transaction);
    }
}
