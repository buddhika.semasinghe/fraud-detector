package com.buddhika.assignments.exception;

/**
 * Created by Buddhika Semasinghe
 */
public class FraudCreditCardDetectionException extends Exception {
    public FraudCreditCardDetectionException(String message) {
        super(message);
    }
}
