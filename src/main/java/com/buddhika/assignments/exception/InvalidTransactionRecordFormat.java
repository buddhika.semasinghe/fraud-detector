package com.buddhika.assignments.exception;

/**
 * Created by Buddhika Semasinghe
 */
public class InvalidTransactionRecordFormat extends FraudCreditCardDetectionException {
    public InvalidTransactionRecordFormat(String message) {
        super(message);
    }
}
