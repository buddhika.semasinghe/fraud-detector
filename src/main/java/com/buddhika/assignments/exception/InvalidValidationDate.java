package com.buddhika.assignments.exception;

/**
 * Created by Buddhika Semasinghe
 */
public class InvalidValidationDate extends FraudCreditCardDetectionException {
    public InvalidValidationDate(String message) {
        super(message);
    }
}
