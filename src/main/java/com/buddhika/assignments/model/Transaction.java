package com.buddhika.assignments.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Created by Buddhika Semasinghe
 */
public class Transaction {
    private String cardNumber;
    private LocalDateTime transactionDatetime;
    private BigDecimal price;

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public LocalDateTime getTransactionDatetime() {
        return transactionDatetime;
    }

    public void setTransactionDatetime(LocalDateTime transactionDatetime) {
        this.transactionDatetime = transactionDatetime;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
