package com.buddhika.assignments;

import com.buddhika.assignments.exception.FraudCreditCardDetectionException;
import com.buddhika.assignments.util.ServiceValidator;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Buddhika Semasinghe
 */
public class FraudTransactionDetectorIT {

    @Test
    public void testBulkTransactionsInFile() {

        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("input.data.txt").getFile());

        List<String> transactions = new ArrayList();
        try (Scanner scanner = new Scanner(file)) {
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                transactions.add(line);
            }
            scanner.close();

        } catch (IOException e) {
            Assert.fail();
        }

        FraudCreditCardDetectionService fraudCreditCardDetectionService = new FraudCreditCardDetectionService();
        ServiceValidator serviceValidator = new ServiceValidator();
        fraudCreditCardDetectionService.setServiceValidator(serviceValidator);
        try {
            List<String> cardNumbers = fraudCreditCardDetectionService.process(transactions,
                    "2012-10-06", new BigDecimal("1500.00"));
            Assert.assertEquals(2, cardNumbers.size());
        } catch (FraudCreditCardDetectionException e) {
            Assert.fail();
        }
    }
}
