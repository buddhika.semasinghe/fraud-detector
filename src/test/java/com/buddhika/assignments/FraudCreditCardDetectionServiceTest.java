package com.buddhika.assignments;

import com.buddhika.assignments.exception.FraudCreditCardDetectionException;
import com.buddhika.assignments.model.Transaction;
import com.buddhika.assignments.util.ServiceValidator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

/**
 * Created by Buddhika Semasinghe
 */
@RunWith(MockitoJUnitRunner.class)
public class FraudCreditCardDetectionServiceTest {
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Mock
    ServiceValidator validator;

    @InjectMocks
    FraudCreditCardDetectionService classTobeTested = null;

    private BigDecimal priceThreshold;
    private List<String> transactionRecords;
    private String comparedDate;

    @Before
    public void runBefore() {
        MockitoAnnotations.initMocks(this);

        classTobeTested = new FraudCreditCardDetectionService();
        transactionRecords = new ArrayList<>();
        comparedDate = "2014-04-29";
        priceThreshold = null;
        classTobeTested.setServiceValidator(validator);
    }


    @Test
    public void testThresholdNotExceededForSingleTransaction() throws FraudCreditCardDetectionException {

        List<Transaction> transactions = new ArrayList<>();
        Transaction transaction = Mockito.mock(Transaction.class);
        transactions.add(transaction);

        when(validator.validateDate(comparedDate)).thenReturn(LocalDate.parse("2014-04-29"));
        when(validator.validateTransactionRecords(transactionRecords)).thenReturn(transactions);
        when(transaction.getTransactionDatetime()).thenReturn(LocalDateTime.parse("2014-04-29T13:15:54", DateTimeFormatter.ISO_LOCAL_DATE_TIME));
        when(transaction.getCardNumber()).thenReturn("10d7ce2f43e35fa57d1bbf8b1e2");
        when(transaction.getPrice()).thenReturn(new BigDecimal(10.00));

        transactionRecords.add("10d7ce2f43e35fa57d1bbf8b1e2, 2014-04-29T13:15:54, 10.00");
        priceThreshold = new BigDecimal(20);
        Assert.assertEquals(0, classTobeTested.process(transactionRecords, comparedDate, priceThreshold).size());
    }

    @Test
    public void testThresholdExceededForSingleTransaction() throws FraudCreditCardDetectionException {

        List<Transaction> transactions = new ArrayList<>();
        transactions.add(createValidValidTransaction("10d7ce2f43e35fa57d1bbf8b1e2",
                "2014-04-29T13:15:54", "10.00"));

        when(validator.validateDate(comparedDate)).thenReturn(LocalDate.parse("2014-04-29"));
        when(validator.validateTransactionRecords(transactionRecords)).thenReturn(transactions);

        transactionRecords.add("10d7ce2f43e35fa57d1bbf8b1e2, 2014-04-29T13:15:54, 10.00");
        priceThreshold = new BigDecimal(9);
        Assert.assertEquals("10d7ce2f43e35fa57d1bbf8b1e2", classTobeTested.process(transactionRecords, comparedDate, priceThreshold).get(0));
    }

    @Test
    public void testThresholdExceededForMultipleTransactionsPerDayForSingleCard() throws FraudCreditCardDetectionException {

        List<Transaction> transactions = new ArrayList<>();
        transactions.add(createValidValidTransaction("10d7ce2f43e35fa57d1bbf8b1e2",
                "2014-04-29T13:15:54", "10.00"));
        transactions.add(createValidValidTransaction("10d7ce2f43e35fa57d1bbf8b1e2",
                "2014-04-29T13:16:54", "11.00"));

        when(validator.validateDate(comparedDate)).thenReturn(LocalDate.parse("2014-04-29"));
        when(validator.validateTransactionRecords(transactionRecords)).thenReturn(transactions);

        transactionRecords.add("10d7ce2f43e35fa57d1bbf8b1e2, 2014-04-29T13:15:54, 10.00");
        transactionRecords.add("10d7ce2f43e35fa57d1bbf8b1e2, 2014-04-29T13:16:54, 11.00");
        priceThreshold = new BigDecimal(20);
        Assert.assertEquals("10d7ce2f43e35fa57d1bbf8b1e2", classTobeTested.process(transactionRecords, comparedDate, priceThreshold).get(0));
    }

    @Test
    public void testThresholdNotExceededForMultipleTransactionsPerDayForSingleCard() throws FraudCreditCardDetectionException {

        List<Transaction> transactions = new ArrayList<>();
        transactions.add(createValidValidTransaction("10d7ce2f43e35fa57d1bbf8b1e2",
                "2014-04-29T13:15:54", "20.00"));
        transactions.add(createValidValidTransaction("10d7ce2f43e35fa57d1bbf8b1e2",
                "2014-04-29T13:16:54", "11.00"));

        when(validator.validateDate(comparedDate)).thenReturn(LocalDate.parse("2014-04-29"));
        when(validator.validateTransactionRecords(transactionRecords)).thenReturn(transactions);

        transactionRecords.add("10d7ce2f43e35fa57d1bbf8b1e2, 2014-04-29T13:15:54, 20.00");
        transactionRecords.add("10d7ce2f43e35fa57d1bbf8b1e2, 2014-04-29T13:16:54, 11.00");
        priceThreshold = new BigDecimal(40);
        Assert.assertEquals(0, classTobeTested.process(transactionRecords, comparedDate, priceThreshold).size());
    }

    @Test
    public void testThresholdExceededForMultipleTransactionsPerDayForMultipleCards() throws FraudCreditCardDetectionException {

        List<Transaction> transactions = new ArrayList<>();
        transactions.add(createValidValidTransaction("10d7ce2f43e35fa57d1bbf8b1e2",
                "2014-04-29T13:15:54", "20.00"));
        transactions.add(createValidValidTransaction("10d7ce2f43e35fa57d1bbf8b1e1",
                "2014-04-29T13:16:54", "11.00"));

        when(validator.validateDate(comparedDate)).thenReturn(LocalDate.parse("2014-04-29"));
        when(validator.validateTransactionRecords(transactionRecords)).thenReturn(transactions);

        transactionRecords.add("10d7ce2f43e35fa57d1bbf8b1e2, 2014-04-29T13:15:54, 20.00");
        transactionRecords.add("10d7ce2f43e35fa57d1bbf8b1e1, 2014-04-29T13:16:54, 11.00");
        priceThreshold = new BigDecimal(10);
        Assert.assertEquals(2, classTobeTested.process(transactionRecords, comparedDate, priceThreshold).size());
    }

    @Test
    public void testOneOfTheCardsExceededTheThreshold() throws FraudCreditCardDetectionException {

        List<Transaction> transactions = new ArrayList<>();
        transactions.add(createValidValidTransaction("10d7ce2f43e35fa57d1bbf8b1e2",
                "2014-04-29T13:15:54", "20.00"));
        transactions.add(createValidValidTransaction("10d7ce2f43e35fa57d1bbf8b1e1",
                "2014-04-29T13:16:54", "11.00"));

        when(validator.validateDate(comparedDate)).thenReturn(LocalDate.parse("2014-04-29"));
        when(validator.validateTransactionRecords(transactionRecords)).thenReturn(transactions);

        transactionRecords.add("10d7ce2f43e35fa57d1bbf8b1e2, 2014-04-29T13:15:54, 20.00");
        transactionRecords.add("10d7ce2f43e35fa57d1bbf8b1e1, 2014-04-29T13:16:54, 11.00");
        priceThreshold = new BigDecimal(15);
        Assert.assertEquals("10d7ce2f43e35fa57d1bbf8b1e2", classTobeTested.process(transactionRecords, comparedDate, priceThreshold).get(0));
    }

    @Test
    public void testMultipleTransactionsDuringDifferentDaysNotExceedThreshold() throws FraudCreditCardDetectionException {

        List<Transaction> transactions = new ArrayList<>();
        transactions.add(createValidValidTransaction("10d7ce2f43e35fa57d1bbf8b1e3",
                "2014-04-26T13:16:54", "20.00"));
        transactions.add(createValidValidTransaction("10d7ce2f43e35fa57d1bbf8b1e3",
                "2014-04-27T13:16:54", "15.00"));
        transactions.add(createValidValidTransaction("10d7ce2f43e35fa57d1bbf8b1e3",
                "2014-04-29T13:16:54", "18.00"));

        when(validator.validateDate(comparedDate)).thenReturn(LocalDate.parse("2014-04-27"));
        when(validator.validateTransactionRecords(transactionRecords)).thenReturn(transactions);

        transactionRecords.add("10d7ce2f43e35fa57d1bbf8b1e3, 2014-04-26T13:15:54, 20.00");
        transactionRecords.add("10d7ce2f43e35fa57d1bbf8b1e3, 2014-04-27T13:16:54, 11.00");
        transactionRecords.add("10d7ce2f43e35fa57d1bbf8b1e3, 2014-04-29T13:16:54, 11.00");
        priceThreshold = new BigDecimal(15);
        Assert.assertEquals(0, classTobeTested.process(transactionRecords, comparedDate, priceThreshold).size());
    }

    @Test
    public void testMultipleTransactionsDuringDifferentDaysExceedThreshold() throws FraudCreditCardDetectionException {

        List<Transaction> transactions = new ArrayList<>();
        transactions.add(createValidValidTransaction("10d7ce2f43e35fa57d1bbf8b1e3",
                "2014-04-26T13:16:54", "60.00"));
        transactions.add(createValidValidTransaction("10d7ce2f43e35fa57d1bbf8b1e3",
                "2014-04-27T13:16:54", "34.00"));
        transactions.add(createValidValidTransaction("10d7ce2f43e35fa57d1bbf8b1e3",
                "2014-04-29T13:16:54", "25.00"));

        transactions.add(createValidValidTransaction("10d7ce2f43e35fa57d1bbf8b1e4",
                "2014-04-26T13:16:54", "30.00"));
        transactions.add(createValidValidTransaction("10d7ce2f43e35fa57d1bbf8b1e4",
                "2014-04-29T13:16:54", "20.00"));


        when(validator.validateDate(comparedDate)).thenReturn(LocalDate.parse("2014-04-29"));
        when(validator.validateTransactionRecords(transactionRecords)).thenReturn(transactions);

        transactionRecords.add("10d7ce2f43e35fa57d1bbf8b1e3, 2014-04-26T13:15:54, 60.00");
        transactionRecords.add("10d7ce2f43e35fa57d1bbf8b1e3, 2014-04-27T13:16:54, 34.00");
        transactionRecords.add("10d7ce2f43e35fa57d1bbf8b1e3, 2014-04-29T13:16:54, 25.00");

        transactionRecords.add("10d7ce2f43e35fa57d1bbf8b1e4, 2014-04-27T13:16:54, 30.00");
        transactionRecords.add("10d7ce2f43e35fa57d1bbf8b1e4, 2014-04-29:16:54, 20.00");
        priceThreshold = new BigDecimal(19);
        Assert.assertEquals(2, classTobeTested.process(transactionRecords, comparedDate, priceThreshold).size());
    }


    private Transaction createValidValidTransaction(String hashedCardNumber, String dateTime, String price) {
        Transaction transaction = Mockito.mock(Transaction.class);
        when(transaction.getTransactionDatetime()).thenReturn(LocalDateTime.parse(dateTime, DateTimeFormatter.ISO_LOCAL_DATE_TIME));
        when(transaction.getCardNumber()).thenReturn(hashedCardNumber);
        when(transaction.getPrice()).thenReturn(new BigDecimal(price));
        return transaction;
    }
}
