package com.buddhika.assignments.util;

import com.buddhika.assignments.exception.InvalidTransactionRecordFormat;
import com.buddhika.assignments.exception.InvalidValidationDate;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Buddhika Semasinghe
 */
public class ServiceValidatorTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();
    private ServiceValidator classToBeTested;
    private List<String> transactions;

    @Before
    public void runBefore() {
        classToBeTested = new ServiceValidator();
        transactions = new ArrayList<>();
    }

    @Test
    public void testReturnExceptionForNullValidationDate() throws InvalidValidationDate {
        thrown.expect(InvalidValidationDate.class);
        thrown.expectMessage("Empty or Null Compared Date");
        classToBeTested.validateDate(null);
    }

    @Test
    public void testReturnExceptionForEmptyValidationDate() throws InvalidValidationDate {
        thrown.expect(InvalidValidationDate.class);
        thrown.expectMessage("Empty or Null Compared Date");
        classToBeTested.validateDate("");
    }

    @Test
    public void testReturnExceptionForInvalidValidationDateFormat() throws InvalidValidationDate {
        thrown.expect(InvalidValidationDate.class);
        thrown.expectMessage("Invalid date format");
        classToBeTested.validateDate("2013-425-34");
    }


    @Test
    public void testNullTransactionsReturnException() throws InvalidTransactionRecordFormat {
        thrown.expect(InvalidTransactionRecordFormat.class);
        thrown.expectMessage("Null or Empty input Transactions");
        classToBeTested.validateTransactionRecords(null);
    }

    @Test
    public void testEmptyCollectionsOfTransactionsReturnException() throws InvalidTransactionRecordFormat {
        thrown.expect(InvalidTransactionRecordFormat.class);
        thrown.expectMessage("Null or Empty input Transactions");
        classToBeTested.validateTransactionRecords(transactions);
    }

    @Test
    public void testInvalidListOfTransactionsReturnsEmptyList() throws InvalidTransactionRecordFormat {
        transactions.add("10d7ce2f43e35fa57d1bbf8b1e2");
        Assert.assertEquals(0, classToBeTested.validateTransactionRecords(transactions).size());

        transactions.clear();
        transactions.add("10d7ce2f43e35fa57d1bbf8b1e2, ");
        Assert.assertEquals(0, classToBeTested.validateTransactionRecords(transactions).size());

        transactions.clear();
        transactions.add("10d7ce2f43e35fa57d1bbf8b1e2, 2014-04-29T13:15:54, ");
        Assert.assertEquals(0, classToBeTested.validateTransactionRecords(transactions).size());

        transactions.clear();
        transactions.add("10d7ce2f43e35fa57d1bbf8b1e2, 2014-04-29T13:15:54, 345, 34 ");
        Assert.assertEquals(0, classToBeTested.validateTransactionRecords(transactions).size());
    }

    @Test
    public void testValidSingleTransactionsReturnsWithOneElementList() throws InvalidTransactionRecordFormat {
        transactions.add("10d7ce2f43e35fa57d1bbf8b1e2, 2014-04-29T13:15:54, 10.00");
        Assert.assertEquals(1, classToBeTested.validateTransactionRecords(transactions).size());
    }

    @Test
    public void testMultipleTransactionsReturnsWithOneElementList() throws InvalidTransactionRecordFormat {
        transactions.add("10d7ce2f43e35fa57d1bbf8b1e2, 2014-04-29T13:15:54, 10.00");
        transactions.add("10d7ce2f43e35fa57d1bbf8b1e2, 2014-04-29T13:15:54");
        Assert.assertEquals(1, classToBeTested.validateTransactionRecords(transactions).size());
    }

    @Test
    public void testMultipleTransactionsReturnsWithMultipleElementList() throws InvalidTransactionRecordFormat {
        transactions.add("10d7ce2f43e35fa57d1bbf8b1e2, 2014-04-29T13:15:54, 10.00");
        transactions.add("10d7ce2f43e35fa57d1bbf8b1e2, 2014-04-29T13:16:54, 200.34");
        Assert.assertEquals(2, classToBeTested.validateTransactionRecords(transactions).size());
    }

}